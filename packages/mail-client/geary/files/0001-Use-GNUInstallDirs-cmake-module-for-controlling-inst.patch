From a321ece2cd0c33ba1b60560db606775b41b94fda Mon Sep 17 00:00:00 2001
From: Julian Ospald <hasufell@posteo.de>
Date: Sat, 21 May 2016 13:20:26 +0200
Subject: [PATCH] Use GNUInstallDirs cmake module for controlling install
 directories
Upstream: https://bugzilla.gnome.org/show_bug.cgi?id=766746

This is useful if 'bin' and 'share' don't have the same prefix,
allowing things like:
* /usr/share/geary/...
* /usr/<host-prefix>/bin/geary
---
 CMakeLists.txt         |  2 ++
 desktop/CMakeLists.txt |  4 ++--
 help/CMakeLists.txt    |  2 +-
 icons/CMakeLists.txt   | 22 +++++++++++-----------
 sql/CMakeLists.txt     |  2 +-
 src/CMakeLists.txt     |  2 +-
 theming/CMakeLists.txt |  2 +-
 7 files changed, 19 insertions(+), 17 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 5aa1b19..5ea9475 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -28,6 +28,8 @@ set(ARCHIVE_BASE_NAME ${CMAKE_PROJECT_NAME}-${VERSION})
 set(ARCHIVE_FULL_NAME ${ARCHIVE_BASE_NAME}.tar.xz)
 set(ARCHIVE_DEBUILD_FULL_NAME ${CMAKE_PROJECT_NAME}_${VERSION}.orig.tar.xz)
 
+include(GNUInstallDirs)
+
 option(DEBUG "Build for debugging." OFF)
 option(ICON_UPDATE "Run gtk-update-icon-cache after the install." ON)
 option(DESKTOP_UPDATE "Run update-desktop-database after the install." ON)
diff --git a/desktop/CMakeLists.txt b/desktop/CMakeLists.txt
index 127911f..4509d4e 100644
--- a/desktop/CMakeLists.txt
+++ b/desktop/CMakeLists.txt
@@ -2,7 +2,7 @@
 # Build and install geary.desktop
 #
 
-install(FILES geary.appdata.xml DESTINATION share/appdata)
+install(FILES geary.appdata.xml DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/appdata)
 
 include (FindIntltool)
 include (FindDesktopFileValidate)
@@ -60,5 +60,5 @@ else (DISABLE_CONTRACT)
             endif (DESKTOP_FILE_VALIDATE_FOUND)
         endif (DESKTOP_VALIDATE)
     endif (INTLTOOL_MERGE_FOUND)
-    install (PROGRAMS geary-attach DESTINATION bin)
+    install (PROGRAMS geary-attach DESTINATION ${CMAKE_INSTALL_BINDIR})
 endif (DISABLE_CONTRACT)
diff --git a/help/CMakeLists.txt b/help/CMakeLists.txt
index 8ba094c..6056f75 100644
--- a/help/CMakeLists.txt
+++ b/help/CMakeLists.txt
@@ -23,7 +23,7 @@ set(TRANSLATED
     it
 )
 
-set(HELP_DEST share/gnome/help/geary)
+set(HELP_DEST ${CMAKE_INSTALL_DATAROOTDIR}/gnome/help/geary)
 
 set(HELP_SOURCE)
 foreach(_page ${HELP_FILES})
diff --git a/icons/CMakeLists.txt b/icons/CMakeLists.txt
index b14903d..0d8af11 100644
--- a/icons/CMakeLists.txt
+++ b/icons/CMakeLists.txt
@@ -1,4 +1,4 @@
-set(ICONS_DEST share/icons/hicolor/scalable/actions)
+set(ICONS_DEST ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor/scalable/actions)
 
 set(ICON_FILES
     mail-archive-symbolic.svg
@@ -27,21 +27,21 @@ set(ICON_FILES
 install(FILES ${ICON_FILES} DESTINATION ${ICONS_DEST})
 
 # Application icon goes in theme directory
-install(FILES "hicolor/16x16/apps/geary.png" DESTINATION share/icons/hicolor/16x16/apps)
-install(FILES "hicolor/24x24/apps/geary.png" DESTINATION share/icons/hicolor/24x24/apps)
-install(FILES "hicolor/32x32/apps/geary.png" DESTINATION share/icons/hicolor/32x32/apps)
-install(FILES "hicolor/48x48/apps/geary.png" DESTINATION share/icons/hicolor/48x48/apps)
-install(FILES "hicolor/256x256/apps/geary.png" DESTINATION share/icons/hicolor/256x256/apps)
-install(FILES "hicolor/512x512/apps/geary.png" DESTINATION share/icons/hicolor/512x512/apps)
-install(FILES "hicolor/symbolic/apps/geary-symbolic.svg" DESTINATION share/icons/hicolor/symbolic/apps)
+install(FILES "hicolor/16x16/apps/geary.png" DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor/16x16/apps)
+install(FILES "hicolor/24x24/apps/geary.png" DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor/24x24/apps)
+install(FILES "hicolor/32x32/apps/geary.png" DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor/32x32/apps)
+install(FILES "hicolor/48x48/apps/geary.png" DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor/48x48/apps)
+install(FILES "hicolor/256x256/apps/geary.png" DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor/256x256/apps)
+install(FILES "hicolor/512x512/apps/geary.png" DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor/512x512/apps)
+install(FILES "hicolor/symbolic/apps/geary-symbolic.svg" DESTINATION ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor/symbolic/apps)
 
 # Optional: update icon cache at install time.
 if (ICON_UPDATE)
     install(
         CODE
-            "execute_process (COMMAND gtk-update-icon-cache -t -f ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor)"
+            "execute_process (COMMAND gtk-update-icon-cache -t -f ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor)"
         CODE
-            "message (STATUS \"Updated icon cache in ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor\")"
+            "message (STATUS \"Updated icon cache in ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor\")"
     )
     
     add_custom_target(
@@ -49,7 +49,7 @@ if (ICON_UPDATE)
         COMMAND
             gtk-update-icon-cache -t -f ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor
         COMMENT
-            "Updated icon cache after uninstall in ${CMAKE_INSTALL_PREFIX}/share/icons/hicolor"
+            "Updated icon cache after uninstall in ${CMAKE_INSTALL_DATAROOTDIR}/icons/hicolor"
     )
     
     add_dependencies(post-uninstall uninstall-icon-cache)
diff --git a/sql/CMakeLists.txt b/sql/CMakeLists.txt
index 11de86c..aff5458 100644
--- a/sql/CMakeLists.txt
+++ b/sql/CMakeLists.txt
@@ -1,4 +1,4 @@
-set(SQL_DEST share/geary/sql)
+set(SQL_DEST ${CMAKE_INSTALL_DATADIR}/geary/sql)
 
 install(FILES version-001.sql DESTINATION ${SQL_DEST})
 install(FILES version-002.sql DESTINATION ${SQL_DEST})
diff --git a/src/CMakeLists.txt b/src/CMakeLists.txt
index d0e494c..2dfd6d0 100644
--- a/src/CMakeLists.txt
+++ b/src/CMakeLists.txt
@@ -621,7 +621,7 @@ OPTIONS
 
 add_executable(geary ${GEARY_VALA_C} ${RESOURCES_C})
 target_link_libraries(geary m ${DEPS_LIBRARIES} gthread-2.0 geary-static)
-install(TARGETS geary RUNTIME DESTINATION bin)
+install(TARGETS geary RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
 add_custom_command(
     TARGET
         geary
diff --git a/theming/CMakeLists.txt b/theming/CMakeLists.txt
index d5b3a13..69f5d3c 100644
--- a/theming/CMakeLists.txt
+++ b/theming/CMakeLists.txt
@@ -1,4 +1,4 @@
-set(THEMING_DEST share/geary/theming)
+set(THEMING_DEST ${CMAKE_INSTALL_DATADIR}/geary/theming)
 
 install(FILES message-viewer.html DESTINATION ${THEMING_DEST})
 install(FILES message-viewer.css DESTINATION ${THEMING_DEST})
-- 
2.8.3

